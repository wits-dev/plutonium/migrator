using System;
using System.Collections.Generic;
using Npgsql;

namespace Plutonium.Migrator.Helper
{
    internal class DatabaseHelper
    {
        private readonly NpgsqlConnection _connection;

        public DatabaseHelper(NpgsqlConnection connection)
        {
            _connection = connection;
        }

        public void EnsureMigrationTableExists()
        {
            const string script = @"
                CREATE TABLE IF NOT EXISTS __pu_migrations_history (
                    id serial primary key,
                    name    text,
                    applied_at timestamp with time zone NOT NULL DEFAULT now(),
                    rolledback_at timestamp with time zone
                );
            ";

            Execute(script);
        }

        public string[] ListAppliedMigrations()
        {
            const string query = @"SELECT name FROM __pu_migrations_history
                                    WHERE rolledback_at IS NULL
                                    ORDER BY name";

            List<string> migrations = new List<string>();
            using (var cmd = new NpgsqlCommand(query, _connection))
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    migrations.Add(reader.GetString(0));
                }
            }

            return migrations.ToArray();
        }

        public void ApplyMigration(string name, string migrationsScript)
        {
            var query = "INSERT INTO __pu_migrations_history (name) VALUES (@name);"
                    + migrationsScript;

            Execute(query, ("name", name));
        }

        public void RollbackMigration(string name, string rollbackScript)
        {
            var query = @"UPDATE __pu_migrations_history
                            SET
                                rolledback_at = now()
                            WHERE name = @name
                            AND rolledback_at IS NULL
                    ;"
                    + rollbackScript;

            Execute(query, ("name", name));
        }

        public void Execute(string query, params (string, object)[] parameters)
        {
            using (var cmd = new NpgsqlCommand(query, _connection))
            {
                foreach (var param in parameters)
                {
                    cmd.Parameters.AddWithValue(param.Item1, param.Item2);
                }

                cmd.ExecuteNonQuery();
            }
        }
    }
}