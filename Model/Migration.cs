using Plutonium.Migrator.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Plutonium.Migrator.Model
{
    public class Migration
    {
        private readonly FileInfo _apply;
        private readonly FileInfo _rollback;

        public string Name { get; }
        public bool Applied { get; }

        public string _applyScript;

        public string ApplyScript
        {
            get
            {
                if(string.IsNullOrWhiteSpace(_applyScript))
                {
                    _applyScript = File.ReadAllText(_apply.FullName, Encoding.UTF8);
                }
                return _applyScript;
            }
        }

        public string _rollbackScript;

        public string RollbackScript
        {
            get
            {
                if(string.IsNullOrWhiteSpace(_rollbackScript))
                {
                    _rollbackScript = File.ReadAllText(_rollback.FullName, Encoding.UTF8);
                }
                return _rollbackScript;
            }
        }

        public Migration(string name, FileInfo apply, FileInfo rollback, bool applied)
        {
            Name = name;
            _apply = apply;
            _rollback = rollback;
            Applied = applied;
        }
    }
}