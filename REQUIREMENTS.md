# Plutonium Migrations Manager

A migration manager for plutonium.

*required input*
- command to run (`apply ${migration-name}`,  `apply all`, `rollback ${migration-name}`, `rollback all`). currently, only `apply all` and `rollback all` will be a permitted for now
- project containing migration files
- database name
- database user
- database password

*startup*
- read migration folder to gather a list migrations and their rollbacks
- ensure that all migrations have a complementary rollback
- rollbacks end with `rollback.sql`
- order the migrations by filename (their natural order)
- check if migration history table exists, if not, create it

*apply*
- when apply is passed, check the last migration from the history that was applied
- check for any migrations between the last applied migration and `${migration-name}` (if `all` then till the most recent migration)
- check if any of the migrations have already been run. if yes, fail.
- in a transaction for each migration, run the migration and store a record in the history table indicating that said migration has been run. include the timestamp.

*rollback*
- when rollback is applied, check to see if `${migration-name}` has been applied already. if not, fail
- select all migrations that have been applied from now backwards until `${migration-name}`. if `all`, then get a full list of all applied migrations.
- in a transaction for each migration, run the rollback migration and indicate the timestamp for the rollback (edited)