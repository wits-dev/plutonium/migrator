# Plutonium.Migrator

Migration utility for apps built on Plutonium


## Instructions

### Usage
 `Plutonium.Migrator [arguments] [options]`


### Arguments

- **Operation** : operation to run
    - `apply` - apply migrations
    - `rollback` - rollback migrations

- **Path**: path to migrations dir

- **ConnectionString**:  _Npgsql_ compatible connection string for database to use

### Options

- `-?|-h|--help`:    show help

- `-m|--migration`  optionally end apply/rollback at this migration (includes migration).
                  by default, all migrations are applied/rolled back

-  `-q|--quiet`      suppress all non-error output


## Example

- Windows
```
set MIGRATION_DIR="./Migrations"

set CONNSTR="Host=localhost;Port=5432;Username=postgres;Password=postgres;Database=escrow;"
```

- Unix/Linux
```
export MIGRATION_DIR=./Migrations

export CONNSTR="Host=localhost;Port=5432;Username=postgres;Password=password;Database=escrow;"
```

#### Apply all migrations

 - `$ dotnet Plutonium.Migrator.dll apply %MIGRATION_DIR% %CONNSTR%`

 - `$ dotnet Plutonium.Migrator.dll apply $MIGRATION_DIR $CONNSTR`
 


#### Apply all migrations up to $MigrationName

 - `$ dotnet Plutonium.Migrator.dll apply $MIGRATION_DIR $CONNSTR -m $MigrationName`

> Applies migrations preceeding and including `$MigrationName` that have not been applied.

#### Rollback all applied migrations

 - `$ dotnet Plutonium.Migrator.dll rollback $MIGRATION_DIR $CONNSTR`


#### Rollback all applied migrations up to $MigrationName

 - `$ dotnet Plutonium.Migrator.dll rollback $MIGRATION_DIR $CONNSTR -m $MigrationName`

> Rolls back all migrations after and including `$MigrationName` that have been applied.


#### Unix/Linux Development

`dotnet run rollback $MIGRATION_DIR $CONNSTR`
`dotnet run apply $MIGRATION_DIR $CONNSTR`

NB: You alternatively publish the project. In this 
you can use the command below.


`$ dotnet Plutonium.Migrator.dll rollback $MIGRATION_DIR $CONNSTR -m $MigrationName`