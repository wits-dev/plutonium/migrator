﻿using Npgsql;
using Plutonium.Migrator.Helper;
using Plutonium.Migrator.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Plutonium.Migrator
{
    internal class MigrationsApp
    {
        private readonly string _path;
        private readonly bool _quietMode;
        public readonly DatabaseHelper _dbHelper;

        private Migration[] _migrations;

        public MigrationsApp(string path, NpgsqlConnection connection, bool quietMode)
        {
            _path = path;
            _quietMode = quietMode;
            _dbHelper = new DatabaseHelper(connection);

            Startup();
        }

        public void Apply(string migrationName)
        {
            Log("Running Apply");

            foreach(var migration in _migrations)
            {
                if(!migration.Applied)
                {
                    Log($"Applying '{migration.Name}'");

                    _dbHelper.ApplyMigration(migration.Name, migration.ApplyScript);
                }

                if(migration.Name == migrationName)
                {
                    break;
                }
            }
        }

        public void Rollback(string migrationName)
        {
            Log("Running Rollback");

            foreach(var migration in _migrations.Reverse()) //we run rollbacks in reverse order
            {
                if(migration.Applied)
                {
                    Log($"Rolling back '{migration.Name}'");

                    _dbHelper.RollbackMigration(migration.Name, migration.RollbackScript);
                }

                if(migration.Name == migrationName)
                {
                    break;
                }
            }
        }

        protected void Startup()
        {
            Log("Running migration startup operations");

            (var applys, var rollbacks) = CheckMigrations();
            var appliedMigrations = CheckDatabase();

            var migrations = new List<Migration>();
            foreach(var apply in applys)
            {
                var name = apply.Key;
                var rollback = rollbacks[name];
                var migration = new Migration(name, apply.Value, rollback, appliedMigrations.Contains(name));

                migrations.Add(migration);
            }

            _migrations = migrations.OrderBy(x => x.Name).ToArray();
        }

        protected string[] CheckDatabase()
        {
            Log("Checking database");

            _dbHelper.EnsureMigrationTableExists();
            return _dbHelper.ListAppliedMigrations();
        }

        protected (Dictionary<string, FileInfo>, Dictionary<string, FileInfo>) CheckMigrations()
        {
            Log("Checking migrations");

            var genPath = Path.Combine(_path, ".pu");
            var generated = new DirectoryInfo(genPath);
            var generatedMigrations = generated.Exists ? generated.GetFiles("*.sql") : new FileInfo[0];

            var custom = new DirectoryInfo(_path);
            if(!custom.Exists)
            {
                throw new Exception($"Path not found : {_path}");
            }
            var customMigrations = custom.GetFiles("*.sql");

            var allMigrations = generatedMigrations.ToDictionary(x => x.Name, x => x);
            foreach(var migration in customMigrations)
            {
                allMigrations[migration.Name] = migration;
            }

            if(allMigrations.Count == 0)
            {
                throw new Exception($"No migrations found in path : {_path}");
            }

            var applys = allMigrations
                .Where(x => x.Key.EndsWith(".up.sql"))
                .OrderBy(x => x.Key)
                .ToDictionary(x => x.Key.Replace(".up.sql", ""), x => x.Value);

            var rollbacks = allMigrations
                .Where(x => x.Key.EndsWith(".down.sql"))
                .OrderBy(x => x.Key)
                .ToDictionary(x => x.Key.Replace(".down.sql", ""), x => x.Value);

            var unmatchedApplys = applys
                .Keys
                .Except(rollbacks.Keys);
            if(unmatchedApplys.Any())
            {
                throw new Exception($"The following migrations have no rollbacks : [{string.Join(",", unmatchedApplys)}]");
            }

            var unmatchedRollbacks = rollbacks
                .Keys
                .Except(applys.Keys);
            if(unmatchedRollbacks.Any())
            {
                throw new Exception($"The following rollbacks have no migrations : [{string.Join(",", unmatchedRollbacks)}]");
            }

            return (
                applys,
                rollbacks
            );
        }

        protected void Log(string msg)
        {
            if(!_quietMode)
            {
                Console.WriteLine($"------------------  {msg}");
            }
        }
    }
}