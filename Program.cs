﻿using McMaster.Extensions.CommandLineUtils;
using Npgsql;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Plutonium.Migrator
{
    [Command(Name = "Plutonium.Migrator", Description = "Migration utility for apps built on Plutonium")]
    [HelpOption("-?|-h|--help", Description = "show help")]
    internal class Program
    {
        private static int Main(string[] args)
        {
            return CommandLineApplication.Execute<Program>(args);
        }

        [Argument(0, Description = @"operation to run
        apply - apply migrations
        rollback - rollback migrations
        execute - execute an sql file
        ")]
        private string Operation { get; }

        [Argument(1, Description = "path to migrations dir or sql file")]
        private string Path { get; }

        [Argument(2, Description = "Npgsql compatible connection string")]
        private string ConnectionString { get; }

        [Option("-m|--migration", Description = @"optionally end apply/rollback at this migration (includes migration).
        by default, all migrations are applied/rolled back")]
        private string Migration { get; } = "all";

        [Option("-q|--quiet", Description = "suppress all non-error output")]
        private bool QuietMode { get; }

        private int OnExecute(CommandLineApplication app)
        {
            if(!new string[] { "apply", "rollback" , "execute" }.Contains(Operation)
                || string.IsNullOrWhiteSpace(Path)
                || string.IsNullOrWhiteSpace(ConnectionString))
            {
                app.ShowHelp();
                return 0;
            }

            try
            {
                using(var connection = new NpgsqlConnection(ConnectionString))
                {
                    connection.Open();

                    switch(Operation)
                    {
                        case "apply":
                            new MigrationsApp(Path, connection, QuietMode).Apply(Migration);
                            break;

                        case "rollback":
                            new MigrationsApp(Path, connection, QuietMode).Rollback(Migration);
                            break;

                        case "execute":
                            new SqlApp(Path, connection, QuietMode).Execute();
                            break;
                    }

                    return 0;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return 1;
        }
    }
}