﻿using Npgsql;
using Plutonium.Migrator.Helper;
using Plutonium.Migrator.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;

namespace Plutonium.Migrator
{
    internal class SqlApp
    {
        private readonly string _path;
        private readonly bool _quietMode;
        public readonly DatabaseHelper _dbHelper;

        private FileInfo[] Files;

        public SqlApp(string path, NpgsqlConnection connection, bool quietMode)
        {
            _path = path;
            _quietMode = quietMode;
            _dbHelper = new DatabaseHelper(connection);

            Startup();
        }

        public void Execute()
        {

            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                foreach(var file in Files)
                {
                    Log($"Executing: {file.Name}");
                    _dbHelper.Execute(File.ReadAllText(file.FullName, Encoding.UTF8));
                }

                scope.Complete();
            }
        }

        protected void Startup()
        {
            Log("Running sql startup operations");

            Files = ListFiles();
        }

        protected FileInfo[] ListFiles()
        {
            Log("Listing sql files");

            var file = new FileInfo(_path);
            if(File.Exists(_path)) {
                return new FileInfo[] { file };
            }

            
            var directory = new DirectoryInfo(_path);
            if(!directory.Exists)
            {
                throw new Exception($"Path does not exist: {_path}");
            }

            var files = directory.GetFiles("*.sql");
            if(files.Length == 0)
            {
                throw new Exception($"Path is empty : {_path}");
            }

            return files;
        }

        protected void Log(string msg)
        {
            if(!_quietMode)
            {
                Console.WriteLine($"------------------  {msg}");
            }
        }
    }
}