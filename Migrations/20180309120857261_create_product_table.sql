

CREATE TABLE public."product"
(
  id serial NOT NULL,
  name text NOT NULL, 
  description text NULL, 
  type text NOT NULL, 
  created_by integer NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_by integer NOT NULL,
  updated_at timestamp with time zone NOT NULL DEFAULT now(),
  deleted_by integer,
  deleted_at timestamp with time zone,
  CONSTRAINT product_pkey PRIMARY KEY (id),  
  CONSTRAINT product_name_check_len CHECK (char_length(name) >= 2 AND char_length(name) <= 100),
  CONSTRAINT product_description_check_len CHECK (char_length(description) >= 0 AND char_length(description) <= 250),
  CONSTRAINT product_type_check_in CHECK (type = ANY(ARRAY['Goods','Services','Other'])),
  CONSTRAINT product_created_by_fkey FOREIGN KEY (created_by)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT product_updated_by_fkey FOREIGN KEY (updated_by)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT product_deleted_by_fkey FOREIGN KEY (deleted_by)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT

);


CREATE INDEX
  ON public."product" (created_by);
  
CREATE INDEX
  ON public."product" (updated_by);
  
CREATE INDEX
  ON public."product" (deleted_by);



CREATE TABLE public."product_audit_log"
(
  target_product_id integer NOT NULL,
  CONSTRAINT product_audit_log_pkey PRIMARY KEY (id),
  CONSTRAINT product_audit_log_target_product_id_fkey FOREIGN KEY (target_product_id)
      REFERENCES public."product" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT product_audit_log_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
INHERITS (public.audit_log);


CREATE INDEX
  ON public."product_audit_log" (user_id);
  
CREATE INDEX
  ON public."product_audit_log" (target_product_id);
  
--- permissions

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('product.add', 'Add product');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('product.fetch', 'Retrieve product created by the current user');
    
INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('product.fetch.su', 'Retrieve product created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('product.getLogs', 'Retrieve logs for product created by the current user');
    
INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('product.getLogs.su', 'Retrieve logs for product created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('product.list', 'List products created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('product.list.su', 'List products created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('product.restore', 'Restore a trashed product created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('product.restore.su', 'Restore a trashed product created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('product.trash', 'Trash product created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('product.trash.su', 'Trash product created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('product.update', 'Update product created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('product.update.su', 'Update product created by any user');
