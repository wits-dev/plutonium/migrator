

CREATE TABLE public."voucher_twelve_reserve"
(
  id bigserial NOT NULL,
  code text NOT NULL, 
  activated boolean NOT NULL DEFAULT false, 
  CONSTRAINT voucher_twelve_reserve_pkey PRIMARY KEY (id),  
  CONSTRAINT voucher_twelve_reserve_code_key UNIQUE (code),
  CONSTRAINT voucher_twelve_reserve_code_check_len CHECK (char_length(code) = 12)
);


CREATE TABLE public."voucher_sixteen_reserve"
(
  id bigserial NOT NULL,
  code text NOT NULL, 
  activated boolean NOT NULL DEFAULT false, 
  CONSTRAINT voucher_sixteen_reserve_pkey PRIMARY KEY (id),  
  CONSTRAINT voucher_sixteen_reserve_code_key UNIQUE (code),
  CONSTRAINT voucher_sixteen_reserve_code_check_len CHECK (char_length(code) = 16)
);
