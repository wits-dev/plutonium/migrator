

CREATE TABLE public."campaign"
(
  id serial NOT NULL,
  product_id integer NOT NULL, 
  name text NOT NULL, 
  description text NULL, 
  start_date timestamp with time zone NOT NULL, 
  end_date timestamp with time zone NULL, 
  redemption_type text NOT NULL, 
  voucher_length text NOT NULL, 
  enable_insights boolean NOT NULL, 
  active boolean NOT NULL DEFAULT true, 
  created_by integer NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_by integer NOT NULL,
  updated_at timestamp with time zone NOT NULL DEFAULT now(),
  deleted_by integer,
  deleted_at timestamp with time zone,
  CONSTRAINT campaign_pkey PRIMARY KEY (id),  
  CONSTRAINT campaign_name_check_len CHECK (char_length(name) >= 2 AND char_length(name) <= 100),
  CONSTRAINT campaign_description_check_len CHECK (char_length(description) >= 0 AND char_length(description) <= 250),
  CONSTRAINT campaign_redemption_type_check_in CHECK (redemption_type = ANY(ARRAY['Single','Unlimited'])),
  CONSTRAINT campaign_voucher_length_check_in CHECK (voucher_length = ANY(ARRAY['Twelve','Sixteen'])),
  CONSTRAINT campaign_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES public."product" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT campaign_created_by_fkey FOREIGN KEY (created_by)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT campaign_updated_by_fkey FOREIGN KEY (updated_by)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT campaign_deleted_by_fkey FOREIGN KEY (deleted_by)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT

);


CREATE INDEX
  ON public."campaign" (created_by);
  
CREATE INDEX
  ON public."campaign" (updated_by);
  
CREATE INDEX
  ON public."campaign" (deleted_by);


CREATE INDEX 
  ON public."campaign" (product_id); 

CREATE TABLE public."campaign_audit_log"
(
  target_campaign_id integer NOT NULL,
  CONSTRAINT campaign_audit_log_pkey PRIMARY KEY (id),
  CONSTRAINT campaign_audit_log_target_campaign_id_fkey FOREIGN KEY (target_campaign_id)
      REFERENCES public."campaign" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT campaign_audit_log_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
INHERITS (public.audit_log);


CREATE INDEX
  ON public."campaign_audit_log" (user_id);
  
CREATE INDEX
  ON public."campaign_audit_log" (target_campaign_id);
  
--- permissions

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('campaign.add', 'Add campaign');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('campaign.fetch', 'Retrieve campaign created by the current user');
    
INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('campaign.fetch.su', 'Retrieve campaign created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('campaign.getLogs', 'Retrieve logs for campaign created by the current user');
    
INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('campaign.getLogs.su', 'Retrieve logs for campaign created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('campaign.list', 'List campaigns created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('campaign.list.su', 'List campaigns created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('campaign.restore', 'Restore a trashed campaign created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('campaign.restore.su', 'Restore a trashed campaign created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('campaign.trash', 'Trash campaign created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('campaign.trash.su', 'Trash campaign created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('campaign.update', 'Update campaign created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('campaign.update.su', 'Update campaign created by any user');
