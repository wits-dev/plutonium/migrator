
DELETE FROM
  public.permission
    WHERE name
    IN (
      'product.add',
      'product.fetch',
      'product.fetch.su',
      'product.getLogs',
      'product.getLogs.su',
      'product.list',
      'product.list.su',
      'product.restore',
      'product.restore.su',
      'product.trash',
      'product.trash.su',
      'product.update',
      'product.update.su'
    );

DROP TABLE public."product_audit_log";
DROP TABLE public."product";

