

DELETE from public."permission_role"
WHERE role_id = 2
AND permission_id IN 
(
	29,
	30,
	32,
	34,
	40,
	42,
	43,
	45,
	47,
	53,
	55,
	56,
	58,
	60,
	66,
	68
);


UPDATE public."user"
SET username = 'admin'
WHERE id = 2
