
DELETE FROM
  public.permission
    WHERE name
    IN (
      'campaign.add',
      'campaign.fetch',
      'campaign.fetch.su',
      'campaign.getLogs',
      'campaign.getLogs.su',
      'campaign.list',
      'campaign.list.su',
      'campaign.restore',
      'campaign.restore.su',
      'campaign.trash',
      'campaign.trash.su',
      'campaign.update',
      'campaign.update.su'
    );

DROP TABLE public."campaign_audit_log";
DROP TABLE public."campaign";

