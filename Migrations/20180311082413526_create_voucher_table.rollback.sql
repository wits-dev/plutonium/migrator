
DELETE FROM
  public.permission
    WHERE name
    IN (
      'voucher.fetch.su',
      'voucher.list.su'
    );

DROP TABLE public."voucher";

