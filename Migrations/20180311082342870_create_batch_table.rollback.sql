
DELETE FROM
  public.permission
    WHERE name
    IN (
      'batch.add',
      'batch.fetch',
      'batch.fetch.su',
      'batch.getLogs',
      'batch.getLogs.su',
      'batch.list',
      'batch.list.su',
      'batch.restore',
      'batch.restore.su',
      'batch.trash',
      'batch.trash.su',
      'batch.update',
      'batch.update.su'
    );

DROP TABLE public."batch_audit_log";
DROP TABLE public."batch";

