


ALTER TABLE public.user_profile
  ADD COLUMN phone_number text NOT NULL default '';

ALTER TABLE public.user_profile
  ADD COLUMN country text NOT NULL default '';


ALTER TABLE public.user_profile
   ALTER COLUMN phone_number DROP DEFAULT;

ALTER TABLE public.user_profile
   ALTER COLUMN country DROP DEFAULT;


