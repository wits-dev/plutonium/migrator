

CREATE TABLE public."batch"
(
  id serial NOT NULL,
  campaign_id integer NOT NULL, 
  name text NOT NULL, 
  description text NULL, 
  batch_size integer NOT NULL, 
  secu_print boolean NOT NULL DEFAULT false, 
  generated boolean NOT NULL DEFAULT false, 
  created_by integer NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_by integer NOT NULL,
  updated_at timestamp with time zone NOT NULL DEFAULT now(),
  deleted_by integer,
  deleted_at timestamp with time zone,
  CONSTRAINT batch_pkey PRIMARY KEY (id),  
  CONSTRAINT batch_name_check_len CHECK (char_length(name) >= 1 AND char_length(name) <= 100),
  CONSTRAINT batch_description_check_len CHECK (char_length(description) >= 0 AND char_length(description) <= 250),
  CONSTRAINT batch_batch_size_check_lte CHECK (batch_size <= 10000),
  CONSTRAINT batch_campaign_id_fkey FOREIGN KEY (campaign_id)
      REFERENCES public."campaign" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT batch_created_by_fkey FOREIGN KEY (created_by)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT batch_updated_by_fkey FOREIGN KEY (updated_by)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT batch_deleted_by_fkey FOREIGN KEY (deleted_by)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT

);


CREATE INDEX
  ON public."batch" (created_by);
  
CREATE INDEX
  ON public."batch" (updated_by);
  
CREATE INDEX
  ON public."batch" (deleted_by);


CREATE INDEX 
  ON public."batch" (campaign_id); 

CREATE TABLE public."batch_audit_log"
(
  target_batch_id integer NOT NULL,
  CONSTRAINT batch_audit_log_pkey PRIMARY KEY (id),
  CONSTRAINT batch_audit_log_target_batch_id_fkey FOREIGN KEY (target_batch_id)
      REFERENCES public."batch" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT batch_audit_log_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
INHERITS (public.audit_log);


CREATE INDEX
  ON public."batch_audit_log" (user_id);
  
CREATE INDEX
  ON public."batch_audit_log" (target_batch_id);
  
--- permissions

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.add', 'Add batch');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.fetch', 'Retrieve batch created by the current user');
    
INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.fetch.su', 'Retrieve batch created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.getLogs', 'Retrieve logs for batch created by the current user');
    
INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.getLogs.su', 'Retrieve logs for batch created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.list', 'List batches created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.list.su', 'List batches created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.restore', 'Restore a trashed batch created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.restore.su', 'Restore a trashed batch created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.trash', 'Trash batch created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.trash.su', 'Trash batch created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.update', 'Update batch created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.update.su', 'Update batch created by any user');
