

CREATE TABLE public."voucher"
(
  id bigserial NOT NULL,
  batch_id integer NOT NULL, 
  serial_number bigserial NOT NULL, 
  code text NOT NULL, 
  phone_number text NULL, 
  redeemed_at timestamp with time zone NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  CONSTRAINT voucher_pkey PRIMARY KEY (id),  
  CONSTRAINT voucher_code_key UNIQUE (code),
  CONSTRAINT voucher_batch_id_fkey FOREIGN KEY (batch_id)
      REFERENCES public."batch" (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT

);

CREATE INDEX 
  ON public."voucher" (batch_id); 

CREATE INDEX 
  ON public."voucher" (redeemed_at); 


--- Permissions

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('voucher.fetch.su', 'View vouchers. <sensitive>');
	
INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('voucher.list.su', 'List vouchers. <sensitive>');


  
