--- permissions

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.download', 'Download batch created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.download.su', 'Download batch created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.secuprint.download', 'Download SecuPrint enabled batches.');




DELETE FROM
  public.permission
    WHERE name
    IN (
      'batch.restore',
      'batch.restore.su',
      'batch.trash',
      'batch.trash.su'
    );
	