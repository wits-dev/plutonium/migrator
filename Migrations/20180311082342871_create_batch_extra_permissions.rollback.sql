
DELETE FROM
  public.permission
    WHERE name
    IN (
      'batch.download',
      'batch.download.su',
      'batch.secuprint.download'
    );


INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.restore', 'Restore a trashed batch created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.restore.su', 'Restore a trashed batch created by any user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.trash', 'Trash batch created by the current user');

INSERT INTO
  public.permission
    (name, description)
  VALUES
    ('batch.trash.su', 'Trash batch created by any user');
